<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/couleursspip?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'colorer_fond' => 'To colour the background',
	'colorer_texte' => 'To colour the text',

	// P
	'pp_couleur_fond' => 'background @couleur@',
	'pp_couleur_icone_fond' => 'B',
	'pp_couleur_icone_texte' => 'T',
	'pp_couleur_texte' => 'Text @couleur@'
);
