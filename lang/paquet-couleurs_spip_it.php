<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-couleurs_spip?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'couleurs_spip_description' => 'Questo plugin consente di modificare l’aspetto di parti di testo. Per colorare determinate parole o modificarne le dimensioni o qualsiasi altra caratteristica tipografica, è sufficiente circondarle con tag di tipo<<code>cs_rouge</code>><cs_rouge>mio testo rosso</cs><<code>/cs</code>>. 
Aggiungendo stili al file css/couleurs_spip.css (in questo caso copialo nella cartella dei template) puoi aggiungere tutti i tag che vuoi.',
	'couleurs_spip_slogan' => 'Testi a colori'
);
