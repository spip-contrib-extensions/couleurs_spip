<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-couleurs_spip?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'couleurs_spip_description' => 'This plugin allow you to change the colour of parts of texts.
	To change the typo of the text (colour, size), just select and add a tag like <<code>cs_rouge</code>><cs_rouge>My red text !!</cs><<code>/cs</code>>. 
	Adding the styles in the file css/couleurs_spip.css - COPY IT in your SQUELETTES/CSS folder, and customized it.
	You can add as many tags you want.',
	'couleurs_spip_slogan' => 'Customize your texts with colours'
);
