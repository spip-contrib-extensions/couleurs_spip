<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/couleursspip?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'colorer_fond' => 'Colora lo sfondo',
	'colorer_texte' => 'Colora il testo',

	// P
	'pp_couleur_fond' => 'Sfondo @couleur@',
	'pp_couleur_icone_fond' => 'S',
	'pp_couleur_icone_texte' => 'T',
	'pp_couleur_texte' => 'Testo @couleur@'
);
