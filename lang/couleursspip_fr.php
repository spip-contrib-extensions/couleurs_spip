<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/couleurs_spip.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'colorer_fond' => 'Colorer le fond',
	'colorer_texte' => 'Colorer le texte',

	// P
	'pp_couleur_fond' => 'Fond @couleur@',
	'pp_couleur_icone_fond' => 'F',
	'pp_couleur_icone_texte' => 'T',
	'pp_couleur_texte' => 'Texte @couleur@'
);
